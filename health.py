# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys
import unicodedata
from datetime import datetime, timedelta, date

from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval, Not, Bool
from trytond.pool import Pool, PoolMeta


__all__ = [
        'Patient','PartyPatient', 
        'PatientData', 'Appointment'
        ]

class Patient(metaclass = PoolMeta):
    __name__ = 'gnuhealth.patient'
    

class PartyPatient(metaclass = PoolMeta):
    __name__ = 'party.party'
    
    non_identified_renaper = fields.Boolean('No identificado en sisa',
                                            readonly=True)
    
    identified_sisa_icon = \
        fields.Function(
            fields.Char('Identified Sisa Icon'),
                'get_identified_sisa_icon'
                )
    
    last_sisa_census_check = fields.Date('Last SISA census check',
                                             readonly= True)
    
    def get_identified_sisa_icon(self,name):
        if self.identified_renaper and self.insurance:
            return 'renaper_puco_icon'
        elif self.identified_renaper:
            return 'renaper_icon'
        elif self.insurance:
            return 'puco_icon'        
        if self.non_identified_renaper:
            return 'no_renaper_icon'
    
    def get_dod(self,name):
        if self.deceased:
            if self.death_certificate:
                return self.death_certificate.dod
        #TODO use an extra field named dod_aprox on this table
        #and return it here
        day = datetime.now().day
        month = datetime.now().month
        year = datetime.now().year
        hour = datetime.now().hour
        minute = datetime.now().minute
        second = datetime.now().second
        return datetime(day=day, month=month, year= year, hour=hour, minute=minute, second=second)        

    @classmethod
    def __setup__(cls):
        super(PartyPatient,cls).__setup__()
        cls.dod.help = u'Fecha de deceso o bien fecha de averiguación de deceso'
        cls.dod.states['invisible'] = True
    
class PatientData(metaclass = PoolMeta):
    __name__ = 'gnuhealth.patient'
    
    identified_sisa_icon = \
        fields.Function(
            fields.Char('Identified Sisa Icon'),
                'get_identified_sisa_icon'
                )
    
    identified_renaper = fields.Function(
                        fields.Boolean('Identificado en SISA'),
                        'get_identified_renaper',
                        searcher='search_identified_renaper')
    
    non_identified_renaper = fields.Function(
                        fields.Boolean('No identificado en SISA'),
                        'get_non_identified_renaper',
                        searcher='search_non_identified_renaper')
    
    last_sisa_census_check = fields.Function(
                        fields.Date('Ultima consulta a SISA'),
                        'get_last_sisa_census_check',
                        searcher='search_last_sisa_census_check')    
    
    def get_identified_sisa_icon(self,name):
        if self.name.identified_renaper and self.current_insurance:
            return 'renaper_puco_icon'
        elif self.name.identified_renaper:
            return 'renaper_icon'
        elif self.current_insurance:
            return 'puco_icon'
        elif self.non_identified_renaper:
            return 'no_renaper_icon'
    
    def get_dod(self, name):
        return self.name.dod
   
    def get_cod(self, name):
        if (self.deceased):
            if self.name.death_certificate:
                return self.name.death_certificate.cod.id
        return None
    
    def get_identified_renaper(self,name):
        if self.name.identified_renaper:
            return True
        return False
    
    @classmethod
    def search_identified_renaper(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('name.identified_renaper', clause[1], value))
        return res
    
    def get_non_identified_renaper(self,name):
        if self.name.non_identified_renaper:
            return True
        return False
    
    @classmethod
    def search_non_identified_renaper(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('name.non_identified_renaper', clause[1], value))
        return res
    
    def get_last_sisa_census_check(self,name):
        if self.name.last_sisa_census_check:
            return self.name.last_sisa_census_check
        return None
    
    @classmethod
    def search_last_sisa_census_check(cls,name,clause):
        res = []
        value = clause[2]
        res.append(('name.last_sisa_census_check',clause[1],value))
        return res
    
    @classmethod
    def __setup__(cls):
        super(PatientData,cls).__setup__()
        cls.dod.help = u'Fecha de deceso o bien fecha de averiguación de deceso'
        cls.dod.states['invisible'] = True
        cls.cod.states['invisible'] = True

class Appointment(metaclass = PoolMeta):
    'Patient Appointments'
    __name__ = 'gnuhealth.appointment'
    
    identified_sisa_icon = \
        fields.Function(
            fields.Char('Identified Sisa Icon'),
                'get_identified_sisa_icon'
                )
    
    def get_identified_sisa_icon(self,name):
        if self.patient and self.patient.name.identified_renaper and self.patient.current_insurance:
            return 'renaper_puco_icon'
        elif self.patient and self.patient.name.identified_renaper:
            return 'renaper_icon'
        elif self.patient and self.patient.current_insurance:
            return 'puco_icon'
