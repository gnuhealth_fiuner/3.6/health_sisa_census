# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys
import unicodedata

from datetime import datetime, date

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, StateAction, \
    Button
from trytond.pool import Pool
from trytond.pyson import Eval, Bool, Not

from ..census_ws import CensusWS
from trytond.modules.health_sisa_puco.puco_ws import PucoWS


__all__ = ['PatientCreateStart', 'PatientCreateFound', 'PatientCreateManual', 
           'PatientCreateData', 'PatientCreate']


class PatientCreateStart(ModelView):
    'Patient Create Start'
    __name__ = 'gnuhealth.patient.create.start'

    nro_documento = fields.Char('ID number')
    sexo = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ], 'Gender')
    phone = fields.Char('Telephone')
    du = fields.Many2One('gnuhealth.du','Unidad Domiciliaria',
                         help=u'Buscar si ya esta registrado en el sistema\n'
                              u'Dejar vacío si desea obtenerlo de Renaper')


class PatientCreateFound(ModelView):
    'Patient Create Found'
    __name__ = 'gnuhealth.patient.create.found'
    
    ref = fields.Char('PUID', readonly=True)
    identificado_renaper = fields.Char('RENAPER code', readonly=True)
    padron_sisa = fields.Char('SISA census', readonly=True)
    codigo_sisa = fields.Char('SISA code', readonly=True)
    tipo_documento = fields.Char('ID type', readonly=True)
    nro_documento = fields.Char('ID number', readonly=True)
    apellido = fields.Char('Lastname', readonly=True)
    nombre = fields.Char('Name', readonly=True)
    sexo = fields.Char('Sex', readonly=True)
    fecha_nacimiento = fields.Date('Birth date', readonly=True)
    estado_civil = fields.Char('Marital status', readonly=True)
    provincia = fields.Char('Province', readonly=True)
    departamento = fields.Char('Subdivision', readonly=True)
    localidad = fields.Char('City', readonly=True)
    domicilio = fields.Char('Address', readonly=True)
    piso_depto = fields.Char('Apartment', readonly=True)
    codigo_postal = fields.Char('Zip code', readonly=True)
    pais_nacimiento = fields.Char('Birth country', readonly=True)
    provincia_nacimiento = fields.Char('Birth province', readonly=True)
    localidad_nacimiento = fields.Char('Birth city', readonly=True)
    nacionalidad = fields.Char('Nationality', readonly=True)
    fallecido = fields.Char('Deceased', readonly=True)
    fecha_fallecido = fields.Date('Fecha Fallecido', readonly = True)
    cobertura = fields.One2Many('get.socialsecurity.view', None,'Cobertura',
        readonly=True)
    obra_social_elegida = fields.Many2One('party.party','Obra Social Elegida',                       
        domain=[('id', 'in', Eval('obra_social_domain'))])
    obra_social_domain = fields.Function(
        fields.Many2Many('party.party',None, None, 'Obra social Domain'),
                    'on_change_with_obra_social_domain')
    phone = fields.Char('Telephone', readonly=True)
    select_du = fields.Selection([
        ('',''),
        ('yes',u'Sí'),
        ('no','No'),
        ],'Utilizar ReNaPer para UD',
        help=u'Utilizar los datos provistos por\n'
        u'ReNaPer para la Unidad Domiciliaria',required=True)
    
    url_padron = fields.Char(u'Buscar en el padron web', readonly=True)
    
    @fields.depends('cobertura')
    def on_change_with_obra_social_domain(self, name=None):
        Party = Pool().get('party.party')
        rnos_list = [c.rnos for c in self.cobertura]
        obras_sociales = Party.search([
            ('is_insurance_company','=',True),
            ('identifiers.code','in', rnos_list),
            ])
        return [os.id for os in obras_sociales]    
    
    @staticmethod
    def default_select_du():
        return 'no'
    
    @staticmethod
    def default_url_padron():
        return 'www.saludnqn.gob.ar/PadronConsultasWeb/'


class PatientCreateManual(ModelView):
    'Patient Create Manual'
    __name__ = 'gnuhealth.patient.create.manual'

    apellido = fields.Char('Lastname', required=True)
    nombre = fields.Char('Name', required=True)
    nro_documento = fields.Char('ID number', required=True)
    sexo = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ], 'Gender', required=True)
    fecha_nacimiento = fields.Date('Birth date')
    scanned_id = fields.Boolean('Scanner used to read ID')
    phone = fields.Char('Telephone')    
    du = fields.Many2One('gnuhealth.du','UD',help='Unidad Domiciliaria')

    @fields.depends('apellido')
    def on_change_apellido(self):
        '''
        Example ID to be parsed:
        00305133441"MIAPELLIDO"MI NOMBRE"M"20123456"A"20-12-1950"02-10-2014"207
        '''
        code = self.apellido
        if len(code) > 60:
            elements = code.split('"')
            self.apellido = elements[1]
            self.nombre = elements[2]
            self.nro_documento = elements[4]
            self.sexo = elements[3].lower()
            self.fecha_nacimiento = datetime.strptime(elements[6], '%d-%m-%Y')
            self.scanned_id = True


class PatientCreateData(ModelView):
    'Patient Create Data'
    __name__ = 'gnuhealth.patient.create.data'

    apellido = fields.Char('Lastname', readonly=True)
    nombre = fields.Char('Name', readonly=True)
    nro_documento = fields.Char('ID number', readonly=True)
    sexo = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ], 'Gender', readonly=True)
    fecha_nacimiento = fields.Date('Birth date', readonly=True)


class PatientCreate(Wizard):
    'Patient Create'
    __name__ = 'gnuhealth.patient.create'
    
    @classmethod
    def __setup__(cls):
        super(PatientCreate, cls).__setup__()
        cls._error_messages.update({
            'NO_PATIENT_ID': 'No patient ID',
            'PATIENT_ALREADY_EXISTS': 'The patient "%(patient)s" '
            'with id "%(puid)s" already exists in database',
            'ERROR_WS': 'SISA web service error',
            'ERROR_AUTENTICACION': 'User authentication error',
            'ERROR_INESPERADO': 'Unexpected error',
            'NO_TIENE_QUOTA_DISPONIBLE': 'User has no asigned quota',
            'ERROR_DATOS': 'Remote call error',
            'REGISTRO_NO_ENCONTRADO': 'Citizen not found in RENAPER',
            'SERVICIO_RENAPER_NO_DISPONIBLE': 'RENAPER service unavailable',
            'MULTIPLE_RESULTADO': 'More than one result, '
            'please set the gender and then try again',
            'unknown_error': 'Unknown error',
        })    

    start_state = 'start'
    start = StateView('gnuhealth.patient.create.start',
        'health_sisa_census.view_patient_create_start', [
            Button('Check census', 'check_sisa', 'tryton-connect',
                default=True),
            Button('Manual creation', 'manual', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    check_sisa = StateTransition()
    found = StateView('gnuhealth.patient.create.found',
        'health_sisa_census.view_patient_create_found', [
            Button('Create patient', 'create_patient', 'tryton-ok',
                default=True),
            Button('Search again', 'start', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    manual = StateView('gnuhealth.patient.create.manual',
        'health_sisa_census.view_patient_create_manual', [
            Button('Create patient', 'show_data', 'tryton-ok',
                default=True),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    show_data = StateTransition()
    data = StateView('gnuhealth.patient.create.data',
        'health_sisa_census.view_patient_create_data', [
            Button('Create patient', 'create_manually', 'tryton-ok',
                default=True),
            Button('Search again', 'manual', 'tryton-ok'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    create_patient = StateAction('health.action_gnuhealth_patient_view')
    create_manually = StateAction('health.action_gnuhealth_patient_view')

    def transition_check_sisa(self):
        Party = Pool().get('party.party')

        id_number = self.start.nro_documento.replace('.','').replace(',','')        
        sex = self.start.sexo or ''
        ref = id_number
        party = Party.search(
                    ['OR',
                     ('ref','=',id_number),
                     ('ref','=',id_number+sex),
                     ])
        if (len(party) > 0) and\
            (len([x for x in party if x.gender == sex])>0):
            self.raise_user_error('PATIENT_ALREADY_EXISTS', {
                'patient': ', '.join([party[0].lastname,party[0].name]),
                'puid': id_number,
                })
        elif (len(party) > 0) and\
            (len([x for x in party if x.gender != sex])>0):
            ref = id_number + sex
            party = []
        
        if party:
            self.raise_user_error('PATIENT_ALREADY_EXISTS', {
                'patient': ', '.join([party[0].lastname,party[0].name]),
                'puid': id_number,
                })
        xml = CensusWS.get_xml(id_number, sex)
        xml_puco = PucoWS.get_xml(id_number)
        if xml is None:
            self.raise_user_error('ERROR_WS')
        xml_result = xml.findtext('resultado')
        print('\n\n\n\n\n',ref)
        if  xml_result == 'OK':
            self.found.ref = ref
            self.found.identificado_renaper = \
                xml.findtext('identificadoRenaper')
            self.found.padron_sisa = xml.findtext('PadronSISA')
            self.found.codigo_sisa = xml.findtext('codigoSISA')
            self.found.tipo_documento = xml.findtext('tipoDocumento')
            self.found.nro_documento = xml.findtext('nroDocumento')
            self.found.apellido = xml.findtext('apellido')
            self.found.nombre = xml.findtext('nombre')
            self.found.sexo = xml.findtext('sexo')
            self.found.fecha_nacimiento = xml.findtext('fechaNacimiento')
            self.found.estado_civil = xml.findtext('estadoCivil')
            self.found.provincia = xml.findtext('provincia')
            self.found.departamento = xml.findtext('departamento')
            self.found.localidad = xml.findtext('localidad')
            self.found.domicilio = xml.findtext('domicilio')
            self.found.piso_depto = xml.findtext('pisoDpto')
            self.found.codigo_postal = xml.findtext('codigoPostal')
            self.found.pais_nacimiento = xml.findtext('paisNacimiento')
            self.found.provincia_nacimiento = \
                xml.findtext('provinciaNacimiento')
            self.found.localidad_nacimiento = \
                xml.findtext('localidadNacimiento')
            self.found.nacionalidad = xml.findtext('nacionalidad')
            self.found.fallecido = xml.findtext('fallecido')
            self.found.fecha_fallecido = xml.findtext('fechaFallecido')
            cobertura = []
            osocial = {}
            for cober in xml.findall('cobertura'):
                osocial['tipoCobertura'] = cober.findtext('tipoCobertura')
                osocial['nombreObraSocial'] = cober.findtext('nombreObraSocial')
                osocial['rnos']= cober.findtext('rnos')
                osocial['procedencia'] = 'padron'
                osocial['vigenciaDesde '] = cober.findtext('vigenciaDesde')
                osocial['fechaActualizacion'] = cober.findtext('fechaActualizacion')
                osocial['denominacion'] = cober.findtext('denominacion') or ''
                cobertura.append(osocial)
                osocial = {}
            if xml_puco and xml_puco.findtext('resultado') == 'OK':
                for cober in xml_puco.findall('puco'):
                    osocial['tipoCobertura'] = cober.findtext('tipoCobertura')
                    osocial['nombreObraSocial'] = cober.findtext('coberturaSocial')
                    osocial['rnos'] = cober.findtext('rnos')
                    osocial['procedencia'] = 'puco'
                    osocial['vigenciaDesde '] = cober.findtext('vigenciaDesde') or ''
                    osocial['fechaActualizacion'] = cober.findtext('fechaActualizacion') or ''
                    osocial['denominacion'] = cober.findtext('denominacion')
                    cobertura.append(osocial)
                    osocial = {}
            self.found.cobertura = cobertura
            self.found.phone = self.start.phone
            return 'found'
        elif xml_result in\
            ['ERROR_AUTENTICACION','ERROR_INESPERADO','NO_TIENE_QUOTA_DISPONIBLE',
             'ERROR_DATOS','REGISTRO_NO_ENCONTRADO','SERVICIO_RENAPER_NO_DISPONIBLE',
             'MULTIPLE_RESULTADO']:
            self.raise_user_error(xml_result)
            
        else:
            self.raise_user_error('unknown_error')

    def default_manual(self, fields):
        return {
            'scanned_id': False,
            }

    def default_data(self, fields):
        return {
            'apellido': self.manual.apellido,
            'nombre': self.manual.nombre,
            'nro_documento': self.manual.nro_documento,
            'sexo': self.manual.sexo,
            'fecha_nacimiento': self.manual.fecha_nacimiento,
            }

    def default_found(self, fields):
        pool = Pool()
        Party = pool.get('party.party')
        cobertura = []
        osocial = {}
        for cober in self.found.cobertura:
            osocial['tipoCobertura'] = cober.tipoCobertura
            osocial['nombreObraSocial'] = cober.nombreObraSocial
            osocial['rnos']= cober.rnos           
            osocial['vigenciaDesde'] = cober.vigenciaDesde\
                if hasattr(cober,'vigenciaDesde') else None
            osocial['fechaActualizacion'] = cober.fechaActualizacion\
                if hasattr(cober,'fechaActualizacion') else None
            osocial['procedencia'] = cober.procedencia
            osocial['denominacion'] = cober.denominacion
            cobertura.append(osocial.copy())
            osocial = {}
        obra_social_elegida = None
        if cobertura and\
            (sum([x['rnos'] == cobertura[0]['rnos'] for x in cobertura]) == len(cobertura)):
            os = Party.search([
                ('is_insurance_company','=',True),
                ('identifiers.code','=', cobertura[0]['rnos'])                    
                ])
            if os:
                obra_social_elegida = os[0].id
        return {
            'ref': self.found.ref,
            'identificado_renaper': self.found.identificado_renaper,
            'padron_sisa': self.found.padron_sisa,
            'codigo_sisa': self.found.codigo_sisa,
            'tipo_documento': self.found.tipo_documento,
            'nro_documento': self.found.nro_documento,
            'apellido': self.found.apellido,
            'nombre': self.found.nombre,
            'sexo': self.found.sexo,
            'fecha_nacimiento': self.found.fecha_nacimiento,
            'estado_civil': self.found.estado_civil,
            'provincia': self.found.provincia,
            'departamento': self.found.departamento,
            'localidad': self.found.localidad,
            'domicilio': self.found.domicilio,
            'piso_depto': self.found.piso_depto,
            'codigo_postal': self.found.codigo_postal,
            'pais_nacimiento': self.found.pais_nacimiento,
            'provincia_nacimiento': self.found.provincia_nacimiento,
            'localidad_nacimiento': self.found.localidad_nacimiento,
            'nacionalidad': self.found.nacionalidad,
            'fallecido': self.found.fallecido,
            'fecha_fallecido': self.found.fecha_fallecido,
            'cobertura': cobertura,
            'obra_social_elegida': obra_social_elegida,
            'phone': self.found.phone,
            'fallecido': self.found.fallecido,
            }

    def do_create_patient(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Address = pool.get('party.address')
        Contact_Mechanism = pool.get('party.contact_mechanism')
        DU = pool.get('gnuhealth.du')
        Country = pool.get('country.country')
        Subdivision = pool.get('country.subdivision')
        Insurance = pool.get('gnuhealth.insurance')
        Patient = pool.get('gnuhealth.patient')

        identifier_data = {
            'type': 'ar_dni' if self.found.tipo_documento == 'DNI' else None,
            'code': self.found.nro_documento,
            }

        party_data = {
            'name': self.found.nombre,
            'lastname': self.found.apellido,
            'ref': self.found.ref,
            'identifiers': [('create', [identifier_data])],
            'dob': self.found.fecha_nacimiento,
            'gender': self.found.sexo.lower() if self.found.sexo else 'm',
            'identified_renaper': True,
            'renaper_id': self.found.identificado_renaper,
            'is_person': True,
            'is_patient': True,
            'non_identified_renaper': False,
            'deceased': True if self.found.fallecido == 'SI' else False,
            'date_of_deceased': self.found.fecha_fallecido,
            'last_sisa_census_check': date.today(),
            }
        if self.found.padron_sisa == 'SI':
            party_data['identified_sisa'] = True
            party_data['sisa_code'] = self.found.codigo_sisa
        # 'party_ar' module requires vat_number definition
        if 'trytond.modules.party_ar' in sys.modules:
            party_data['iva_condition'] = 'consumidor_final'

        if self.found.pais_nacimiento:
            country = Country.search(
                ['OR',
                 ('name', 'ilike', self.found.pais_nacimiento),
                 ('name', 'ilike', self.found.nacionalidad),
                 ])
            if country:
                party_data['citizenship'] = country[0].id
        party_data['federation_account'] = self.found.nro_documento
        party = Party.create([party_data])
        if self.found.phone:
                contact_mechanism = Contact_Mechanism.create([{
                    'party': party[0].id,
                    'type': 'phone', 
                    'value': self.found.phone
                    }])        
        patient = None

        if party:
            if self.found.domicilio:
                normal_provincia = ''.join((c for c in unicodedata.normalize('NFD', self.found.provincia)
                                            if unicodedata.category(c) != 'Mn'))
                subdivision = Subdivision.search([('name','ilike',normal_provincia)])
                country = Country.search([
                    'OR',
                    ('name','ilike',self.found.pais_nacimiento),
                    ('name','ilike',self.found.nacionalidad),
                    ]) or Country.search([('name','ilike','argentina'),])
                
                du = None
                if self.start.du:
                    du = self.start.du
                if self.found.select_du == 'yes':
                    du = DU.search([
                        ('name','=',self.found.domicilio+ ' '+\
                            self.found.localidad+ ' ('+self.found.apellido+')')
                        ])
                    if not du:
                        du = DU.create([{
                                'name':self.found.domicilio+ ' '
                                    +self.found.localidad+' ('+self.found.apellido+')',
                                'address_street': self.found.domicilio,
                                'address_city': self.found.localidad_nacimiento,
                                'address_subdivision': subdivision[0].id,
                                'address_country': country[0].id,                 
                                }])
                    party[0].du = du[0].id
                party[0].save()
                direccion = Address().search([
                    ('party', '=', party[0].id),
                    ])

                if direccion and (direccion[0].street is None
                        or direccion[0].street == ''):
                    direccion[0].update_direccion(party[0], self.found)
                else:
                    direccion = Address()
                    direccion.update_direccion(party[0], self.found)

            insurance = None
            if self.found.obra_social_elegida:
                insurance_party = Party().search([
                    ('id', '=', self.found.obra_social_elegida.id),
                    ])
                #if not insurance_party:
                    #insurance_party = Party().search([
                        #('is_insurance_company', '=', True),
                        #('name', '=', self.found.obra_social),
                        #])
                if insurance_party:
                    insurance = Insurance().search([
                        ('name', '=', party[0].id),
                        ('company', '=', insurance_party[0].id),
                        ])
                    if not insurance:
                        insurance_data = {
                            'name': party[0].id,
                            'number': self.found.nro_documento,
                            'company': insurance_party[0].id,
                            'insurance_type':
                                insurance_party[0].insurance_company_type,
                            }
                        insurance = Insurance.create([insurance_data])

            patient = Patient.create([{
                'name': party[0],
                }])
            if patient and insurance:
                if not patient[0].current_insurance:
                    patient[0].current_insurance = insurance[0]
                    #patient[0].current_insurance.member_since = self.found.vigencia_os
                    patient[0].save()

        data = {}
        if patient:
            data = {'res_id': [p.id for p in patient]}
            if len(patient) == 1:
                action['views'].reverse()
        return action, data

    def transition_show_data(self):
        if self.manual.scanned_id:
            return 'data'
        return 'create_manually'

    def do_create_manually(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Contact_Mechanism = pool.get('party.contact_mechanism')
        Patient = pool.get('gnuhealth.patient')

        identifier_data = {
            'type': 'ar_dni',
            'code': self.manual.nro_documento,
            }

        party_data = {
            'name': self.manual.nombre,
            'lastname': self.manual.apellido,
            'ref': self.manual.nro_documento,
            'identifiers': [('create', [identifier_data])],
            'dob': self.manual.fecha_nacimiento,
            'gender': self.manual.sexo,
            'is_person': True,
            'is_patient': True,
            'federation_account': self.manual.nro_documento,
            }
        # 'party_ar' module requires vat_number definition
        if 'trytond.modules.party_ar' in sys.modules:
            party_data['iva_condition'] = 'consumidor_final'

        party = Party.create([party_data])
        if party:
            patient = Patient.create([{
                'name': party[0],
                }])
            if self.manual.du:
                party[0].du = self.manual.du
                party[0].save()
            if self.manual.phone:
                contact_mechanism = Contact_Mechanism.create([{
                    'party': party[0].id,
                    'type': 'phone', 
                    'value': self.manual.phone
                    }])

        data = {'res_id': [p.id for p in patient]}
        if len(patient) == 1:
            action['views'].reverse()
        return action, data
