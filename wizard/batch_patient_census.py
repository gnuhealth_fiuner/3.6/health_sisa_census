# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys
import re
from datetime import datetime, date

from ..census_ws import CensusWS
from trytond.modules.health_sisa_puco.puco_ws import PucoWS

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, StateAction, Button
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool, Not

__all__ = ['BatchPatientCensusDataStart', 
           'BatchPatientCensusPrevalidation','BatchPatientCensusPrevalidationList',
           'BatchPatientCensusValidation','BatchPatientCensusValidationList',
           'BatchPatientCensusData','SocialSecurity']

class SocialSecurity(ModelView):
    'Social Security'
    __name__ = 'get.socialsecurity.view'
    
    tipoCobertura = fields.Char('Tipo de Cobertura', readonly=True)    
    nombreObraSocial = fields.Char('Obra Social', readonly=True)
    rnos = fields.Char(u'Código RNOS', readonly=True)
    vigenciaDesde = fields.Char('Vigencia desde', readonly=True)
    fechaActualizacion = fields.Char(u'Fecha actualización', readonly=True)
    denominacion = fields.Char(u'Denominacion', readonly=True)
    procedencia = fields.Selection([
        (None,''),
        ('puco','PUCO'),
        ('padron','Padron',)
        ],'Procedencia',readonly=True)
    
    @staticmethod
    def default_id():
        return '0'


class BatchPatientCensusDataStart(ModelView):
    'Batch Patient Census Data Start'
    __name__ = 'batch.patient.census_data.start'
    
    patient = fields.Many2Many('gnuhealth.patient',
                                None,None,'Pacientes',
                                required = True)


class BatchPatientCensusPrevalidation(ModelView):
    'Batch Patient Census Prevalidation'
    __name__ = 'batch.patient.census.prevalidation'
    
    patient_name = fields.Char('Nombre', readonly = True)
    patient_lastname = fields.Char('Apellido', readonly = True)
    ref = fields.Char(u'Número de documento', readonly = True)
    genero = fields.Selection([
        (None, ''),
        ('m', 'Masculino'),
        ('f', 'Femenino'),
        ], 'Genero', required=True)


class BatchPatientCensusPrevalidationList(ModelView):
    'Batch Patient Census Prevalidation'
    __name__ = 'batch.patient.census.prevalidation_list'
    
    prevalidation_list = fields.One2Many('batch.patient.census.prevalidation',None,
                                  u'Prevalidación', required=True)
    
class BatchPatientCensusValidation(ModelView):
    'Batch Patient Census Validation'
    __name__ = 'batch.patient.census.validation'
    
    update_personal_data = fields.Boolean(
                        'Actualizar Datos Personales',
                            states={
                                'invisible':Not(Bool(Eval('identificado_renaper'))),
                                })
    update_insurance_data = fields.Boolean(
                        'Actualizar Datos Obra social',
                            states={
                                'invisible':Not(Bool(Eval('identificado_renaper'))),
                                })

    update_du = fields.Selection([
        ('', None),
        ('yes', u'Sí'),
        ('no', 'No'),
        ],'Actualizar datos de Unidad Domiciliaria',
        sort = False, required=True)    
    
    ref = fields.Char('PUID', readonly=True)
    identificado_renaper = fields.Char(u'Código RENAPER', readonly=True)
    padron_sisa = fields.Char(u'Padrón SISA', readonly=True)
    codigo_sisa = fields.Char(u'Código SISA', readonly=True)
    tipo_documento = fields.Char('Tipo de Documento', readonly=True)
    nro_documento = fields.Char(u'Número de Documento', readonly=True)
    apellido = fields.Char('Apellido', readonly=True)
    nombre = fields.Char('Nombre', readonly=True)
    sexo = fields.Char('Genero', readonly=True)
    fecha_nacimiento = fields.Date('Fecha de Nacimiento', readonly=True)
    estado_civil = fields.Char('Estado Civil', readonly=True)
    provincia = fields.Char('Provincia', readonly=True)
    departamento = fields.Char('Departamento', readonly=True)
    localidad = fields.Char(u'Ciudad', readonly=True)
    domicilio = fields.Char('Domicilio', readonly=True)
    piso_depto = fields.Char('Departamento del domicilio', readonly=True)
    codigo_postal = fields.Char(u'Código Postal', readonly=True)
    pais_nacimiento = fields.Char(u'País de Nacimiento', readonly=True)
    provincia_nacimiento = fields.Char('Provincia de Nacimiento', readonly=True)
    localidad_nacimiento = fields.Char('Localidad de Nacimiento', readonly=True)
    nacionalidad = fields.Char('Nacionalidad', readonly=True)
    fallecido = fields.Char('Fallecido', readonly=True)
    fecha_fallecido = fields.Date('Fecha de Deceso', readonly=True)
    cobertura = fields.One2Many('get.socialsecurity.view',None,'Cobertura',
        readonly=True)
    obra_social_elegida = fields.Many2One('party.party','Obra Social Elegida',                       
        domain=[('id', 'in', Eval('obra_social_domain'))])
    obra_social_domain = fields.Function(
        fields.Many2Many('party.party',None, None, 'Obra social Domain'),
                    'get_obra_social_domain')
    
    def get_obra_social_domain(self, name=None):
        Party = Pool().get('party.party')
        rnos_list = [c.rnos for c in self.cobertura]          
        obras_sociales = Party.search([
            ('is_insurance_company','=',True),
            ('identifiers.code','in', rnos_list),
            ])
        nombreObraSocial = [c.nombreObraSocial for c in self.cobertura]
        obras_sociales+= Party.search([
            ('is_insurance_company','=',True),
            ('name','in',nombreObraSocial)])
        obras_sociales = list(set(obras_sociales))
        return [os.id for os in obras_sociales]
    
    @fields.depends('cobertura')
    def on_change_with_obra_social_domain(self, name=None):
        Party = Pool().get('party.party')
        rnos_list = [c.rnos for c in self.cobertura]
        obras_sociales = Party.search([
            ('is_insurance_company','=',True),
            ('identifiers.code','in', rnos_list),
            ])
        nombreObraSocial = [c.nombreObraSocial for c in self.cobertura]
        obras_sociales += Party.search([
            ('is_insurance_company','=',True),
            ('name','in',nombreObraSocial)])
        obras_sociales = list(set(obras_sociales))
        return [os.id for os in obras_sociales]
    
    nombre_prevalidado = fields.Char(
                        u"Nombre prevalidado", readonly=True)
    
    apellido_prevalidado = fields.Char(
                        u"Apellido prevalidado", readonly=True)
    
    url_padron = fields.Char(u'Buscar en el padron web', readonly=True)
    
    @staticmethod    
    def default_update_personal_data():
        return False
    
    @staticmethod
    def default_update_insurance_data():
        return False
    
    @staticmethod
    def default_update_du():
        return 'no'
    
    @staticmethod
    def default_url_padron():
        return 'www.saludnqn.gob.ar/PadronConsultasWeb/'
    
class BatchPatientCensusValidationList(ModelView):
    'Batch Patient Census Validation List'
    __name__ = 'batch.patient.census.validation_list'

    validation_list = fields.One2Many('batch.patient.census.validation',
                              None, 'Datos Pacientes')


class BatchPatientCensusData(Wizard):
    'Batch Patient Census Data'
    __name__ = 'batch.patient.census_data'

    @classmethod
    def __setup__(cls):
        super(BatchPatientCensusData, cls).__setup__()
        cls._error_messages.update({
            'more_than_one_patient': 'Please, select only one patient',
            'error_ws': 'SISA web service error',
            'error_autenticacion': 'User authentication error',
            'error_inesperado': 'Unexpected error',
            'no_cuota_disponible': 'User has no asigned quota',
            'error_datos': 'Remote call error',
            'registro_no_encontrado': 'No data found',
            'multiple_resultado': 'More than one result',
            'servicio_no_disponible': 'Service not avaible.\
                            #Try defining the person gender',
        })
    
    start_state = 'start'

    start = StateView(
        'batch.patient.census_data.start',
        'health_sisa_census.batch_patient_census_data_start_view', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'prevalidate', 'tryton-ok', default=True),
        ])
    
    prevalidate = StateTransition()
    
    def transition_prevalidate(self):
        return 'prevalidate_data'
    
    prevalidate_data = StateView(
        'batch.patient.census.prevalidation_list',
        'health_sisa_census.batch_patient_census_data_prevalidation_list_view',[
            Button('Volver','start','tryton-go-previous'),
            Button('Cancel','end','tryton-cancel'),
            Button('Ok','check_census','tryton-ok',default=True),
            ])
        
    def default_prevalidate_data(self,fields):
        data = []
        for patient in self.start.patient:            
            data.append({
                'patient_lastname': patient.name.lastname,
                'patient_name': patient.name.name,
                'ref': patient.name.ref,
                'genero': patient.name.gender,
                })
        return {
            'prevalidation_list': data
            }
    
    check_census = StateTransition()
    
    def transition_check_census(self):
        return 'validate_data'
    
    validate_data = StateView(
        'batch.patient.census.validation_list',
        'health_sisa_census.batch_patient_census_data_validation_list_view',[
            Button('Ok','update_patient','tryton-ok',default=True),
            Button('Volver al inicio','start','tryton-go-previous'),            
            Button('Cancel','end','tryton-cancel'),])
    
    update_patient = StateAction('health.action_gnuhealth_patient_view')

    def default_validate_data(self, fields):
        Patient = Pool().get('gnuhealth.patient')
        Party = Pool().get('party.party')
        validation_list = []
        for patient in self.prevalidate_data.prevalidation_list:
            res = {}
            ref_original = patient.ref
            ref_numeric = re.sub("[^0-9]",'',ref_original)
            xml = CensusWS.get_xml(ref_numeric,patient.genero)
            xml_puco = PucoWS.get_xml(ref_numeric)
            if xml is None:
                res = {
                    'nombre_prevalidado': patient.patient_name,
                    'apellido_prevalidado': patient.patient_lastname,
                    'nro_documento': patient.ref,
                    'nombre': 'Error Web Service SISA',
                    'update_personal_data': False,
                    'update_insurance_data': False,
                    'gender': patient.genero,
                    }
            elif xml.findtext('resultado') == 'OK':
                cobertura = []
                osocial = {}
                for cober in xml.findall('cobertura'):
                    osocial['tipoCobertura'] = cober.findtext('tipoCobertura')
                    osocial['nombreObraSocial'] = cober.findtext('nombreObraSocial')
                    osocial['rnos']= cober.findtext('rnos')                    
                    osocial['vigenciaDesde '] = cober.findtext('vigenciaDesde')
                    osocial['fechaActualizacion'] = cober.findtext('fechaActualizacion')
                    osocial['procedencia'] = 'padron'
                    cobertura.append(osocial.copy())
                    osocial = {}
                if xml_puco and xml_puco.findtext('resultado') == 'OK':
                    for cober in xml_puco.findall('puco'):
                        osocial['nombreObraSocial'] = cober.findtext('coberturaSocial')
                        osocial['rnos'] = cober.findtext('rnos')
                        osocial['denominacion'] = cober.findtext('denominacion')
                        osocial['procedencia'] = 'puco'                        
                        cobertura.append(osocial.copy())
                        osocial = {}
                obra_social_elegida = None
                if cobertura and\
                    (sum([x['rnos'] == cobertura[0]['rnos'] for x in cobertura]) == len(cobertura)):
                    os = Party.search([
                        ('is_insurance_company','=',True),
                        ('identifiers.code','=', cobertura[0]['rnos'])                    
                        ])
                    if os:
                        obra_social_elegida = os[0].id
                res = {
                    'ref': ref_original,
                    'update_personal_data': True,
                    'update_insurance_data': True if cobertura else False,
                    'identificado_renaper': xml.findtext('identificadoRenaper'),
                    'padron_sisa': xml.findtext('PadronSISA'),
                    'codigo_sisa': xml.findtext('codigoSISA'),
                    'tipo_documento': xml.findtext('tipoDocumento'),
                    'nro_documento': xml.findtext('nroDocumento'),
                    'apellido': xml.findtext('apellido'),
                    'nombre': xml.findtext('nombre'),
                    'sexo': xml.findtext('sexo'),
                    'fecha_nacimiento': xml.findtext('fechaNacimiento'),
                    'estado_civil': xml.findtext('estadoCivil'),
                    'provincia': xml.findtext('provincia'),
                    'departamento':xml.findtext('departamento'),
                    'localidad': xml.findtext('localidad'),
                    'domicilio': xml.findtext('domicilio'),
                    'piso_depto': xml.findtext('pisoDpto'),
                    'codigo_postal': xml.findtext('codigoPostal'),
                    'pais_nacimiento': xml.findtext('paisNacimiento'),
                    'provincia_nacimiento': xml.findtext('provinciaNacimiento'),
                    'localidad_nacimiento': xml.findtext('localidadNacimiento'),
                    'nacionalidad': xml.findtext('nacionalidad'),
                    'fallecido': xml.findtext('fallecido'),
                    'fecha_fallecido': xml.findtext('fechaFallecido'),
                    'obra_social_elegida': obra_social_elegida,
                    'cobertura': cobertura,
                    'nombre_prevalidado': patient.patient_name,
                    'apellido_prevalidado': patient.patient_lastname,
                }
            elif xml.findtext('resultado') == 'ERROR_AUTENTICACION':
                res = {
                    'nombre_prevalidado': patient.patient_name,
                    'apellido_prevalidado': patient.patient_lastname,
                    'nro_documento': patient.ref,
                    'nombre': 'Error autenticacion',
                    'update_personal_data': False,
                    'update_insurance_data': False,
                    'sexo': patient.genero,                    
                    }
            elif xml.findtext('resultado') == 'ERROR_INESPERADO':
                res = {
                    'nombre_prevalidado': patient.patient_name,
                    'apellido_prevalidado': patient.patient_lastname,
                    'nro_documento': patient.ref,
                    'nombre': 'Error inesperado',
                    'update_personal_data': False,
                    'update_insurance_data': False,
                    'sexo': patient.genero,                    
                    }
            elif xml.findtext('resultado') == 'NO_TIENE_QUOTA_DISPONIBLE':
                res = {
                    'nombre_prevalidado': patient.patient_name,
                    'apellido_prevalidado': patient.patient_lastname,
                    'nro_documento': patient.ref,
                    'nombre': 'cuota de consultas a SISA agotada',
                    'update_personal_data': False,
                    'update_insurance_data': False,
                    'sexo': patient.genero,
                    }
            elif xml.findtext('resultado') == 'ERROR_DATOS':
                res = {
                    'nombre_prevalidado': patient.patient_name,
                    'apellido_prevalidado': patient.patient_lastname,
                    'nro_documento': patient.ref,
                    'nombre': 'Error de datos de consulta',
                    'update_personal_data': False,
                    'update_insurance_data': False,
                    'sexo': patient.genero,
                    }
            elif xml.findtext('resultado') == 'REGISTRO_NO_ENCONTRADO':
                res = {
                    'nombre_prevalidado': patient.patient_name,
                    'apellido_prevalidado': patient.patient_lastname,                    
                    'nro_documento': str(patient.ref),
                    'nombre': 'Registro no encontrado',
                    'update_personal_data': False,
                    'update_insurance_data': False,
                    'sexo': patient.genero,
                    }                
            elif xml.findtext('resultado') == 'SERVICIO_RENAPER_NO_DISPONIBLE':
                res = {
                    'nombre_prevalidado': patient.patient_name,
                    'apellido_prevalidado': patient.patient_lastname,
                    'nro_documento': patient.ref,
                    'nombre': 'servicio_no_disponible',
                    'update_personal_data': False,
                    'update_insurance_data': False,
                    'sexo': patient.genero,
                    }
            elif xml.findtext('resultado') == 'MULTIPLE_RESULTADO':
                res = {
                    'nombre_prevalidado': patient.patient_name,
                    'apellido_prevalidado': patient.patient_lastname,                    
                    'nro_documento': patient.ref,
                    'nombre': 'Multiple resultado. Intente una busqueda individual',
                    'update_personal_data': False,
                    'update_insurance_data': False,
                    'sexo': patient.genero,
                    }
            validation_list.append(res)

        return {
            'validation_list': validation_list,
            }    

    def do_update_patient(self, action):
        pool = Pool()
        Party = pool.get('party.party')
        Address = pool.get('party.address')
        Country = pool.get('country.country')
        Insurance = pool.get('gnuhealth.insurance')
        Patient = pool.get('gnuhealth.patient')

        patient_list = []        
        for data in self.validate_data.validation_list:
            if data.update_personal_data:
                party = Party.search([('ref','=',data.nro_documento)],limit=1)[0]
                patient = Patient.search([('name','=',party.id)],limit=1)[0]
                identifier_data = {
                    'type': 'ar_dni' if data.tipo_documento == 'DNI' else None,
                    'code': data.nro_documento,
                    }
                party.name = data.nombre
                party.lastname = data.apellido
                party.ref = data.ref                
                if not party.identifiers:
                    party.identifiers+= (identifier_data),                
                if type(data.fecha_nacimiento) is date:
                    party.dob = data.fecha_nacimiento
                elif type(data.fecha_nacimiento) is datetime:
                    party.dob = data.fecha_nacimiento.date()
                elif type(data.fecha_nacimiento) is str:
                    dob = data.fecha_nacimiento[:10]
                    party.dob = datetime.strptime(dob, '%d-%m-%Y').date()
                party.gender = data.sexo.lower() if data.sexo else 'm'
                party.identified_renaper = True
                party.non_identified_renaper = False
                party.last_sisa_census_check = date.today()
                party.renaper_id = data.identificado_renaper
                
                if data.padron_sisa == 'SI':
                    party.identified_sisa = True
                    party.sisa_code = data.codigo_sisa
                if 'trytond.modules.party_ar' in sys.modules:
                    party.iva_condition = 'consumidor_final'

                if data.pais_nacimiento:
                    country = Country.search(
                        ['OR',
                         ('name', 'ilike', data.pais_nacimiento),
                         ('name','ilike',data.nacionalidad),
                         ])                  

                    if country:
                        party.citizenship = country[0].id

                if data.domicilio:
                    direccion = Address().search([
                        ('party', '=', party.id),
                        ])
                    if direccion and (direccion[0].street is None
                            or direccion[0].street == ''):
                        direccion[0].update_direccion(party, data)
                    else:
                        direccion = Address()
                        direccion.update_direccion(party, data)
                
                #update_du = data.update_du
                #if update_du == 'yes':
                    #if patient.du:
                        #du = DU.search('id','=',patient.du.id)
                        #du.street

                #Insurance party, and relation to the patient and party
                insurance = None
                if data.obra_social_elegida and data.update_insurance_data:
                    insurance_party = Party().search([                        
                        ('id', '=', data.obra_social_elegida.id),
                        ])
                    if insurance_party:
                        insurance = Insurance().search([
                            ('name', '=', party.id),#TODO change to party to patient
                            ('number','=',data.nro_documento),
                            ('company', '=', insurance_party[0].id),                            
                            ])
                        if not insurance:
                            insurance_data = {
                                'name': party.id,
                                'number': data.nro_documento,
                                'company': insurance_party[0].id,
                                'insurance_type':
                                    insurance_party[0].insurance_company_type,
                                #'member_since': data.vigencia_os,
                                }
                            insurance = Insurance.create([insurance_data])
                        else:
                            insurance[0].number = data.nro_documento
                            insurance[0].company = insurance_party[0].id
                            insurance[0].insurance_type = insurance_party[0].insurance_company_type
                            #insurance[0].member_since = data.vigencia_os
                            insurance[0].save()
                    party.save()
                    if insurance:                        
                        patient.current_insurance = insurance[0]
                patient.save()
                party.deceased = True if data.fallecido == 'SI' else False
                party.date_of_decesead = data.fecha_fallecido if data.fecha_fallecido else None
                party.save()
                patient_list.append(patient)
            else:                
                party = Party.search([('ref','=',data.nro_documento)],limit=1)[0]                
                party.gender = 'f' if data.sexo == 'F' else 'm'
                party.last_sisa_census_check = date.today()
                party.non_identified_renaper = True\
                    if party.identified_renaper == False else False
                party.save()
                patient = Patient.search([('name','=',party.id)],limit=1)[0]
                patient_list.append(patient)

        data = {'res_id': [p.id for p in patient_list]}
        if len(patient_list) == 1:
            action['views'].reverse()
        return action, data
